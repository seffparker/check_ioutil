# check_ioutil

A Nagios/Icinga plugin written in Bash, which can be used to check and report disk I/O utilization in %, calculated using Kernel data /proc/diskstats

# How it works?

The Linux Kernel keep various I/O statistics in /proc/diskstats in raw format. The script will read the Field 10 (# of milliseconds spent doing I/Os) in specified intervals to determine the %utilization. Read more at https://www.kernel.org/doc/Documentation/iostats.txt

# Configure the script

The script can work without any arguments. The recommended values are declared as variables within the script:

    	WARN=70 # Default warning level
    	CRIT=90 # Default critical level
		POLL_TIME=3 # Default polling time in seconds

We can optionally customize each variables via arguments:

		check_ioutil -w 75 -c 95 -t 5
# Features
 - The script by default exclude devices with name \*ram\*, loop\*, fd\* and sr\*
 - Outputs Performance Data values for graphing.

# Dependencies

The script is written to minimize the use of external binaries. The only external tool which needed is `awk` which is pre-installed in most Linux distributions.

# Debugging

The script has included some codes for debugging purpose. You may need to uncomment them if needed.

# Release Information
 Author: Seff Parker
 
 Version: v 1.1 build 20180808

# Changelog
### v 1.1 build 20180808

 - FIX: Floppy Disks (fd*) are now excluded

### v 1.0 build 20180803

 - Initial Release
