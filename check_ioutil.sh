#!/bin/bash

show_version(){
echo -e "
Nagios/Icinga script to check and report Disk I/O utilization in percentage
Author:  Seff Parker
Version: v1.1 build 20180808
"
}

WARN=70 # Default warning level
CRIT=90 # Default critical level
POLL_TIME=3 # Default polling time in seconds

# Read custom options from arguments
while getopts ":w:c:t:V" opt; do
  case ${opt} in
    w )
      WARN=$OPTARG
      ;;
    c )
      CRIT=$OPTARG
      ;;
    t )
      POLL_TIME=$OPTARG
      ;;
    V )
	show_version
	exit 0
      ;;
    * )
      echo "Usage: $0 [-w Warning] [-c Critical] [-t Poll duration]"
      exit 3
      ;;
  esac
done
shift $((OPTIND -1))

[ $WARN -gt $CRIT ] && echo 'UNKNOWN: Warning level should be lower than critical!' &&  exit 3
[ $POLL_TIME -lt 1 ] && echo 'UNKNOWN: Polling duration should be greater than zero!' && exit 3

SCAN_BEGIN=$(cat /proc/diskstats 2> /dev/null)
sleep $POLL_TIME
SCAN_END=$(cat /proc/diskstats 2> /dev/null)

[ -z "$SCAN_BEGIN" ] && echo 'UNKNOWN: Cannot read disk stats from Kernel!' && exit 3

DISKS=($(echo "$SCAN_BEGIN" | awk '!/loop/ && !/ram/ && !/sr/ && !/fd/ {print $3}'))
MS_BEGIN=($(echo "$SCAN_BEGIN" | awk '!/loop/ && !/ram/ && !/sr/ && !/fd/ {print $13}'))
MS_END=($(echo "$SCAN_END" | awk '!/loop/ && !/ram/ && !/sr/ && !/fd/ {print $13}'))

DISK_NOS=$(echo ${#DISKS[*]})

# Debug:
# echo "Arguments: -w $WARN -c $CRIT -t POLL_TIME"
# echo "Number of disks: $DISK_NOS"
# echo "Disks: ${DISKS[*]}"
# echo "ms begin: ${MS_BEGIN[*]}"
# echo "ms end:   ${MS_END[*]}"

for ((i=0; i<$DISK_NOS; i++))
do
   let UTIL[$i]=("${MS_END[$i]}" - "${MS_BEGIN[$i]}")/${POLL_TIME}0
   SHOW_UTIL[$i]="${DISKS[$i]}: ${UTIL[$i]}%,"
   PERF_DATA[$i]="'${DISKS[$i]}'=${UTIL[$i]}%;$WARN;$CRIT;;"
   [ ${UTIL[$i]} -ge $CRIT ] && CRITICAL=true
   [ ${UTIL[$i]} -ge $WARN ] && WARNING=true
done

if [ $CRITICAL ]
	then echo -n "CRITICAL: Disk I/O Usage is very high! - "
	EXIT=2
elif [ $WARNING ]
	then echo -n "WARNING: Disk I/O Usage is increasing! - "
	EXIT=1
else
	echo -n "OK: Disk I/O usage is normal! - "
	EXIT=0
fi

echo "${SHOW_UTIL[*]}| ${PERF_DATA[*]}"
exit $EXIT